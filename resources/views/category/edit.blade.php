
    <link href="{{ asset('backend/css/styles.css') }}" rel="stylesheet" />

<form method="POST" action="{{ route('category.update',$categories->id)}}">

    @csrf
<div class="mb-3">
    <label for="title" class="form-label">Category Title</label>
    <input name="category_title" type="text" class="form-control" id="title" value="{{$categories->category_title}}">                   
    @error('title')

    <div  class="text-danger">{{ $message }}</div>
    @enderror
</div>

<button type="submit" class="btn btn-primary">Update</button>

</form>