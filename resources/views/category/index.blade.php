


    <link href="{{ asset('backend/css/styles.css') }}" rel="stylesheet" />
{{-- add category --}}
<form method="POST" action="{{route('category.store')}}">

    @csrf
<div class="mb-3">
    <label for="title" class="form-label">Category Title</label>
    <input name="category_title" type="text" class="form-control" id="title" value="">                   
    @error('title')

    <div  class="text-danger">{{ $message }}</div>
    @enderror
</div>

<button type="submit" class="btn btn-primary">Add</button>

</form>


<a href="{{route('category.trash')}}">Trush List</a>






<table border="1">

    <tr>
        <th>SL</th>
        <th>Category Title</th>
        <th>Action</th>

    </tr>

    @foreach($categorydata as $category)
    <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$category->category_title}}</td>
        <td> 

            <a class="btn btn-warning btn-sm" href="{{ route('category.edit', $category->id) }}">Edit</a>    


            <form action="{{ route('category.destroy', $category->id) }}" method="POST" 
                style="display:inline">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach





</table>