


<table border="1">

    <tr>
        <th>SL</th>
        <th>Category Title</th>
        <th>Action</th>

    </tr>

    @foreach($categories as $category)
    <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$category->category_title}}</td>
        <td> 

            <a class="btn btn-warning btn-sm" href="{{ route('category.restore', $category->id) }}">Restore</a>    


            <form action="{{ route('category.delete', $category->id) }}" method="POST" 
                style="display:inline">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure  to  delete parmanently ?')">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach





</table>