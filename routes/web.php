<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::get('/create', [ProductController::class, 'create'])->name('create');
Route::post('/store', [ProductController::class, 'store'])->name('store.product');
Route::get('/show', [ProductController::class, 'show'])->name('show');
Route::get('/delete', [ProductController::class, 'delete'])->name('delete');


Route::prefix('admin')->group(function () {
Route::get('/category', [CategoryController::class, 'create'])->name('category.create');
Route::post('/category/store', [CategoryController::class, 'store'])->name('category.store');

Route::get('/category/index', [CategoryController::class, 'index'])->name('category.index');
Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
Route::delete('/category/{category}/destroy', [CategoryController::class, 'destroy'])->name('category.destroy');
Route::post('/category/{category}/update', [CategoryController::class, 'update'])->name('category.update');
Route::get('/deleted-categories', [CategoryController::class, 'trash'])->name('category.trash');


Route::get('/deleted-categories/{id}/restore', [CategoryController::class, 'restore'])->name('category.restore');
Route::delete('/deleted-categories/{id}', [CategoryController::class, 'delete'])->name('category.delete');


});
